"""
simple functions to compute error bars when having weighted nevents.
The two function provided, compute_scaled_poisson_errorbar and
compute_scaled_poisson_errorbar2, are almost equivalent. The version
compute_scaled_poisson_errorbar2 should be mode precise but less tested.

See at the end for an example for one single bin.
"""

import numpy as np
from scipy import stats
from scipy.optimize import brenth

ALPHA = 1 - 0.6826894921370859


def scale_range(start, start1, start2, end1, end2):
    m = (end2 - end1) / float(start2 - start1)
    return end1 + m * (start - start1)


def compute_poisson_error(n):
    """
    compute the poisson error for a given number of events using RooFit
    """
    import ROOT
    import ctypes

    a = ctypes.c_double(0)
    b = ctypes.c_double(0)
    if abs(n - np.round(n)) < 1e-5:
        ROOT.RooHistError.instance().getPoissonInterval(int(np.round(n)), a, b)
        return n - float(a.value), float(b.value) - n
    else:
        yfloor = np.floor(n).astype(int)
        yceil = yfloor + 1

        ROOT.RooHistError.instance().getPoissonInterval(int(yfloor), a, b)
        floor_error = n - a.value, b.value - n
        ROOT.RooHistError.instance().getPoissonInterval(int(yceil), a, b)
        ceil_error = n - a.value, b.value - n

        lo = scale_range(n, yfloor, yceil, floor_error[0], ceil_error[0])
        hi = scale_range(n, yfloor, yceil, floor_error[1], ceil_error[1])

        return lo, hi


def compute_poisson_error2(n):
    def compute_pvalue2(nobs, ntrue, left):
        if left:
            # the left case (<=) is the cdf
            return poisson_cdf(ntrue, nobs)
        else:
            return 1 - poisson_cdf(ntrue, nobs - 1)  # is this -1 correct ??

    nup = brenth(lambda x: compute_pvalue2(n, x, True) - ALPHA / 2.0, n, n * 10 + 10)
    ndown = brenth(lambda x: compute_pvalue2(n, x, False) - ALPHA / 2.0, 0, n)

    return n - ndown, nup - n


def compute_poisson_error3(n):
    return (
        n - stats.gamma.isf(1 - ALPHA / 2.0, n) if n != 0 else 0.0,
        stats.gamma.isf(ALPHA / 2.0, n + 1) - n,
    )


def poisson_cdf(l, n):
    """
    continuos implementation of the cdf of the poisson distribution
    l: expected value
    n: observed value
    """
    return 1 - stats.gamma(n + 1).cdf(l)


def compute_scaled_poisson_errorbar(nevents, weights, error_poisson_function=compute_poisson_error):
    """
    nevents = list of number of events for each category
    weights = list of weights for each category
    return central value, error down (negative), error up
    """
    nevents = np.asarray(nevents)
    weights = np.asarray(weights)

    nw = float(np.sum(nevents * weights, axis=0))
    nw2 = float(np.sum(nevents * weights**2, axis=0))
    lambda_tilde = nw**2 / nw2
    scale = nw2 / nw

    e = error_poisson_function(lambda_tilde)
    return nw, -e[0] * scale, e[1] * scale


def compute_scaled_poisson_errorbar2(nevents, weights):
    return compute_scaled_poisson_errorbar(nevents, weights, compute_poisson_error2)


def compute_scaled_poisson_errorbar3(nevents, weights):
    return compute_scaled_poisson_errorbar(nevents, weights, compute_poisson_error3)


def compute_scaled_poisson_toys(nevents, weights, ntoys=10000):
    nevents = np.asarray(nevents)
    weights = np.asarray(weights, dtype=float)
    nw_obs = np.sum(nevents * weights)

    def compute_pvalue(mu, upper=True):
        test_true_n = nevents * mu
        test_nw = nw_obs * mu

        toys_ns = stats.poisson(test_true_n).rvs(size=(ntoys, len(weights)))
        toys_nw = np.sum(toys_ns * weights, axis=1)
        if upper:
            pvalue = (toys_nw < nw_obs).sum() / float(ntoys)
        else:
            pvalue = (toys_nw > nw_obs).sum() / float(ntoys)
        return pvalue

    return (
        nw_obs,
        brenth(lambda x: compute_pvalue(x, False) - ALPHA / 2.0, 1e-4, 1.1) * nw_obs - nw_obs,
        brenth(lambda x: compute_pvalue(x, True) - ALPHA / 2.0, 0.9, 10) * nw_obs - nw_obs,
    )


def linear_propagation_gaussian(nevents, weights):
    nevents = np.asarray(nevents)
    weights = np.asarray(weights)

    nw = float(np.sum(nevents * weights, axis=0))
    nw2 = float(np.sum(nevents * weights**2, axis=0))

    linear_error = np.sqrt(nw2)
    return nw, -linear_error, linear_error


def linear_propagation_poisson(nevents, weights):
    nevents = np.asarray(nevents)
    weights = np.asarray(weights)

    nw = float(np.sum(nevents * weights, axis=0))
    errs = np.asarray([np.array(compute_poisson_error(nevent)) for nevent in nevents])
    err_w = (errs.T * weights).T
    linear_error = np.sqrt((err_w**2).sum(axis=0))

    return nw, -linear_error[0], linear_error[1]


if __name__ == "__main__":

    def test(nevents, weights):
        print("nevents = %s, weights = %s" % (nevents, weights))
        methods = (
            compute_scaled_poisson_errorbar,
            compute_scaled_poisson_errorbar2,
            compute_scaled_poisson_errorbar3,
            compute_scaled_poisson_toys,
            linear_propagation_gaussian,
            linear_propagation_poisson,
        )
        for method in methods:
            r = method(nevents, weights)
            print("  %40s: %.5f %.5f" % (method.__name__, r[1], r[2]))
        print()

    test([10, 20, 30], [1, 1, 1])
    test([10, 20, 30], [1, 0, 0])
    test([10, 20, 30], [100, 0, 0])
    test([10, 20, 30], [0.2, 0.1, 0.4])
    test([0, 0, 0, 0, 2, 5, 22], [1.39, 0.55, 0.27, 1.53, 0.55, 0.23, 0.13])
    test([2, 5, 22], [0.55, 0.23, 0.13])
    test(
        [0, 0, 0, 5, 1, 0, 0],
        [0.02044625, 0.05117355, 0.25265265, 0.00132826, 0.00365912, 0.00847892, 0.01714736]
    )
