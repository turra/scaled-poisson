from scaled_poisson import compute_scaled_poisson_errorbar, compute_scaled_poisson_errorbar2, compute_scaled_poisson_errorbar3, compute_scaled_poisson_toys, linear_propagation_gaussian
import ROOT
import argparse

parser = argparse.ArgumentParser(description='Create weighted histograms.')
parser.add_argument('--histograms', nargs='+', help='name of the histograms in the format myfile.root:my_histogram')
parser.add_argument('--weights', nargs='+', type=float, help='the weights')

args = parser.parse_args()

if len(args.histograms) != len(args.weights):
    raise ValueError('number of weights different from the number of histograms')

all_files = {}
histograms = []
for full_path in args.histograms:
    fn, hn = full_path.split(":")
    if fn in all_files:
        f = all_files[fn]
    else:
        f = ROOT.TFile.Open(fn)
        all_files[fn] = f
    h = f.Get(hn)
    print(h)
    if not h:
        raise IOError("cannot find histogram %s in file %s" % (hn, fn))
    histograms.append(h)

for h in histograms:
    if h.GetNbinsX() != histograms[0].GetNbinsX():
        raise ValueError("histograms have different number of bins")


def get_entries(histo):
    return [histo.GetBinContent(ibin) for ibin in range(1, histo.GetNbinsX() + 1)]


def create_weighted_histogram(histograms, weights, merging_function):
    all_entries = [get_entries(histo) for histo in histograms]
    gr = ROOT.TGraphAsymmErrors()
    nbins = len(all_entries[0])

    for ibin in range(nbins):
        entries = [all_entries[ihisto][ibin] for ihisto in range(len(all_entries))]
        value, error_down, error_up = merging_function(entries, weights)
        gr.SetPoint(ibin, histograms[0].GetBinCenter(ibin + 1), value)
        gr.SetPointEYhigh(ibin, error_up)
        gr.SetPointEYlow(ibin, -error_down)

    return gr

colors = ROOT.kRed, ROOT.kOrange, ROOT.kBlue, ROOT.kPink, ROOT.kGreen, ROOT.kViolet
methods = (compute_scaled_poisson_errorbar, compute_scaled_poisson_errorbar2, compute_scaled_poisson_errorbar3, compute_scaled_poisson_toys, linear_propagation_gaussian)
fout = ROOT.TFile('fout.root', 'recreate')
for method, color in zip(methods, colors):
    gr = create_weighted_histogram(histograms, args.weights, method)
    gr.SetMarkerStyle(20)
    gr.SetMarkerColor(color)
    gr.SetLineColor(color)
    gr.SetName(method.__name__)
    gr.Write()
fout.Close()
   
print("use compute_scaled_poisson_errorbar2 histogram in the output")

fout = ROOT.TFile('fout.root')
canvas = ROOT.TCanvas()
legend = ROOT.TLegend(0.1, 0.8, 0.4, 0.9)
for i, method in enumerate(methods):
    gr = fout.Get(method.__name__)
    if i == 0:
        xs = gr.GetX()
        xs.SetSize(gr.GetN())
        xs = list(xs)
        shift = (xs[1] - xs[0]) / float(len(methods)) / 2.
        gr.Draw("AP")
        legend.AddEntry(gr, method.__name__)
    else:
        xs = gr.GetX()
        xs.SetSize(gr.GetN())
        xs = list(xs)
        ys = gr.GetY()
        ys.SetSize(gr.GetN())
        ys = list(ys)
        xs = [xx + shift * i for xx in xs]
        for ipoint, (xx, yy) in enumerate(zip(xs, ys)):
            gr.SetPoint(ipoint, xx, yy)
        gr.Draw("Psame")
        legend.AddEntry(gr, method.__name__)
legend.Draw()        
canvas.SaveAs("comparison.pdf")
                                   
                  

# python scale_histograms.py --histograms mass_points_data15161718_13TeV_TTH_140.0ifb.root:hout_c27 mass_points_data15161718_13TeV_TTH_140.0ifb.root:hout_c28 mass_points_data15161718_13TeV_TTH_140.0ifb.root:hout_c29 mass_points_data15161718_13TeV_TTH_140.0ifb.root:hout_c30 mass_points_data15161718_13TeV_TTH_140.0ifb.root:hout_c31 mass_points_data15161718_13TeV_TTH_140.0ifb.root:hout_c32 mass_points_data15161718_13TeV_TTH_140.0ifb.root:hout_c33 --weights 0.0455 0.1230 0.2748 0.8356 0.1661 0.4035 0.9489   
